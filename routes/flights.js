var express = require('express');
var router = express.Router();
var path = require('path');
const db = require(path.join(__dirname, '../','src/database'))
const Flight = require(path.join(__dirname, '../', 'src/models/flight'))

//get all flights
router.get('/', function (req, res, next){
    //if(req.params.reducedprice==='true'){
//Flight.find({}, { projection: { reducedprice: true } }, function(err, flights) {
    Flight.find(req.query, function(err, flights) {
    //Flight.find({}, function(err, flights) {
        if (err) 
            return console.error(err);
        res.json(flights);  
    });
//}
});

//post new flight
router.post('/newflight', function(req, res, next){
    var flight = new Flight({
        from: req.body.from,
        to: req.body.to,
        price: req.body.price,
        airlines: req.body.airlines,
        reducedprice: req.body.reducedprice,
        dateofdeparture: req.body.dateofdeparture,
        timeofdeparture: req.body.timeofdeparture,
        dateofarrival: req.body.dateofarrival,
        timeofarrival: req.body.timeofarrival, 
        numberofseats: req.body.numberofseats,
    });
  
  flight.save()
    .then(user => {
      res.status(201).json(flight);
    })
    .catch(err => {
      res.status(400).send("unable to save to database");
    });
});


//delete flight
router.delete('/:id', function(req, res, next){
    Flight.findByIdAndRemove(req.params.id, (err, flight) => {
    if (err) return res.status(500).send(err);
    const response = {
        message: "Flight successfully deleted",
        id: flight._id
    };
    return res.status(200).send(response);
});
});

//update flight
router.put('/:id', function(req, res, next){
    Flight.findByIdAndUpdate(
    req.params.id,
    req.body,
    {new: true},
    (err, flight) => {
        if (err) return res.status(500).send(err);
        return res.send(flight);
    }
)
});

module.exports = router;