import React from 'react';
import './App.css';
import { Row, Col, Button } from 'reactstrap';

class Flight extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      price: this.props.price,
      to: this.props.to, 
      imgurl: 'https://london.ac.uk/sites/default/files/styles/promo_mobile/public/2018-10/london-aerial-cityscape-river-thames_1.jpg?itok=ekaaHHpi',
      displayblackhover: 'none',
      displayinfo: 'block'
   };

   this.flightElement = React.createRef();
  }

  sendparams = () => {
    this.props.getidforparams(this.props.from, this.props.to, this.props.price);
  }

  blackhover = () => {
    this.setState({ 
            displayblackhover: 'block',
            displayinfo: 'none'
         });
  }

  removeblackhover = () => {
      this.setState({ 
            displayblackhover: 'none',
            displayinfo: 'block'
         });
  }

  renderflight(){
    return (
        <React.Fragment>
        <Col xs="12" sm="12" md="12" lg="12" xl="12" ref={this.flightElement} style={{backgroundImage: 'url(' + this.state.imgurl + ')',
                                                          backgroundSize: 'cover'}} className="squareimg square" onMouseEnter={this.blackhover} onMouseLeave={this.removeblackhover} >
        
        <div style={{display: this.state.displayblackhover, position: 'absolute', left:0, right:0, top:0, bottom:0, backgroundColor: 'black', opacity: '0.7'}} >
        
        <center>
        <Button outline className="text-center" onClick={this.sendparams} > Book </Button>
        </center>
        </div>
        <Row>
        <Col xs="4" sm="4" md="4" lg="4" xl="4">
          <b>{this.state.price} eur</b>
        </Col>

        <Col xs="4" sm="4" md="4" lg="4" xl="4">
          <b>{this.state.to}</b>
          </Col>

          <Col xs="4" sm="4" md="4" lg="4" xl="4">
          <b>?? cats left</b>
          </Col>
          
          </Row>
          </Col>
      </React.Fragment>
      )
  }

render() { 
          return (this.renderflight())

 }
}
export default Flight