import React from 'react';
import './App.css';
import { Navbar, NavbarBrand } from 'reactstrap';

class NavbarSmall extends React.Component {

render() {
      return (
      	<React.Fragment>
      		<Navbar color="white" light expand="md" style={styles} >
            	<NavbarBrand href="/" className='mr-auto d-none d-sm-block'>catsonboard</NavbarBrand>
      		
       		</Navbar>
       </React.Fragment>
      )
 }  
}
export default NavbarSmall

var styles = {
  backgroundColor:'white !important'
};