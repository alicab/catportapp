import React from 'react';
import './App.css';
import Flight from './flight.js'
import { Container, Row, Col, Jumbotron } from 'reactstrap';

class Flightlist extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      data: [],
      getLoading: true,
      height: 0
   };
  }

  getFlights = ()=>{
    //fetch('/flights?${queryString}', { //akciove letenky na prvej strane
      fetch('/flights?reducedprice=true', {
         method: 'GET'
      })
      .then((response) => response.json())
      .then((responseJson) => { 
         this.setState({
            data: responseJson
         });
      })
      .then(
      () => {
        this.setState({ 
            getLoading: false
         });
        this.setState({ 
            getLoading: true
         });
  })
      .catch((error) => {
         console.error(error);
      });
  }

   componentDidMount(){
      this.getFlights();
}

   sendreducedflightsparams = (from, to, price) => {
      this.props.flightlistoff(from, to);
   }

   renderFlightList = () => {
    return (this.state.data.map((flight) => {return (
        <Col xs="12" sm="6" md="6" lg="3" xl="3" key={flight._id} className='padding_row' >
        <Flight  
              id={flight.id}
              price={flight.price}
              from={flight.from}
              to={flight.to} 
              getidforparams={this.sendreducedflightsparams} />
        </Col>
        )}));
  }

render() {
      return(
        <React.Fragment>
        <Container className="" fluid={true} >
        <Jumbotron style={styles}>
        <p><b>Reduced price flights</b></p>
      </Jumbotron>
        <Row className="no-gutters">
        {this.renderFlightList()}
        </Row>
        </Container>
        </React.Fragment>
        );
   }

}
export default Flightlist;

var styles = {
  backgroundColor:'white'
};