import React from 'react';
import './App.css';
import Searchbardetail from './searchbardetail.js'
import { Container, Row, Col, Button, InputGroup, Input, InputGroupAddon } from 'reactstrap';

class Searchbar extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      to: '',
      from: ''
   };
  }

  changeToInput = (event) => {
      this.setState({
            to: event.target.value
         });
  }

  changeFromInput = (event) => {
        this.setState({
            from: event.target.value
         });
  }

  searchButton = () => {
        this.props.flightlistoff();
  }

  rendersearchbar(){
    return(
      <React.Fragment>
      <Container>
      <Row>
      <Col xs="12" sm="12" md="12" lg="12" xl="12">
      <Searchbardetail />
      </Col>
<     /Row>

      <Row>
      <Col xs="11" sm="11" md="11" lg="11" xl="11"> 
      <Row>
      </Row>
      <Row> 
        <Col xs="6" sm="3" md="3" lg="3" xl="3"> 
         
       <InputGroup>
        <InputGroupAddon addonType="prepend">From</InputGroupAddon>
        <Input placeholder="eg. Brno" />
      </InputGroup>
        </Col>
        <Col xs="6" sm="3" md="3" lg="3" xl="3">  
        <InputGroup>
        <InputGroupAddon addonType="prepend">To</InputGroupAddon>
        <Input placeholder="eg. Berlin" />
      </InputGroup> 
        </Col>
        <Col xs="6" sm="3" md="3" lg="3" xl="3">  
        <InputGroup>
        <InputGroupAddon addonType="prepend">Departure</InputGroupAddon>
        <Input placeholder="" />
      </InputGroup>
        </Col>
        <Col xs="6" sm="3" md="3" lg="3" xl="3">  
        <InputGroup>
        <InputGroupAddon addonType="prepend">Arrival</InputGroupAddon>
        <Input placeholder="" />
      </InputGroup>
        </Col>
        </Row>
      </Col>
      <Col  xs="1" sm="1" md="1" lg="1" xl="1">
        <Col xs="12" sm="12" md="12" lg="12" xl="12">
        <Row>
          <Button className="text-center col-12" onClick={this.searchButton}>Search</Button>
        </Row>
        </Col>
        </Col>
        </Row>
      
        </Container>
      </React.Fragment>
      )
  }

render() { 
          return (this.rendersearchbar())

 }
}
export default Searchbar