import React from 'react';
import './App.css';
import { Row, Col } from 'reactstrap';

class Searchflightdetail extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      from: this.props.from,
      to: this.props.to,
      airlines: this.props.airlines,
      dateofdeparture: this.props.dateofdeparture,
      timeofdeparture: this.props.timeofdeparture,
      dateofarrival: this.props.dateofarrival,
      timeofarrival: this.props.timeofarrival 
   };
  }

  rendersearchflightdetail(){
    return (
        <React.Fragment>

        <Row>

        <Col xs="4" sm="4" md="4" lg="4" xl="4"  >
          {this.state.airlines}
        </Col>

        <Col xs="4" sm="4" md="4" lg="4" xl="4"  >
          <b>{this.state.timeofdeparture}</b>-<b>{this.state.timeofarrival}</b><br/>
          <b>{this.state.dateofdeparture}</b>-<b>{this.state.dateofarrival}</b>
        </Col>

        <Col xs="4" sm="4" md="4" lg="4" xl="4"  >
          doba letu<br/>
          <b>{this.state.from}->{this.state.to}</b>
        </Col>

        </Row>
      </React.Fragment>
      )
  }

render() { 
          return (this.rendersearchflightdetail())

 }
}
export default Searchflightdetail

