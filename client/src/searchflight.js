import React from 'react';
import './App.css';
import Searchflightdetail from './searchflightdetail.js'
import { Row, Col, Jumbotron } from 'reactstrap';

class Searchflight extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      price: this.props.price,
      from: this.props.from,
      to: this.props.to,
      airlines: this.props.airlines,
      dateofdeparture: this.props.dateofdeparture,
      timeofdeparture: this.props.timeofdeparture,
      dateofarrival: this.props.dateofarrival,
      timeofarrival: this.props.timeofarrival
   };
  }

  renderflight(){
    return (
        <React.Fragment><Jumbotron style={styles} >

        <Row>

        <Col xs="3" sm="3" md="3" lg="3" xl="3" className="my-auto">

        <div class="col text-center">
          <b>{this.state.price} €</b>
          </div>
        </Col>

        <Col xs="9" sm="9" md="9" lg="9" xl="9"  >
          <Searchflightdetail from={this.props.from}
                              to={this.props.to}
                              airlines={this.props.airlines}
                              dateofdeparture={this.props.dateofdeparture}
                              timeofdeparture={this.props.timeofdeparture}
                              dateofarrival={this.props.dateofarrival}
                              timeofarrival={this.props.timeofarrival} />
        </Col>
        </Row>
        </Jumbotron>
      </React.Fragment>
      )
  }

render() { 
          return (this.renderflight())

 }
}
export default Searchflight

var styles = {
  backgroundColor:'white'
};
