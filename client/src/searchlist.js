import React from 'react';
import './App.css';
import Searchflight from './searchflight.js'
import { Container, Row, Col } from 'reactstrap';

class Searchlist extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      data: [],
      getLoading: true,
      from: this.props.from,
      to: this.props.to
      //queryString: objToQueryString({
      //  reducedprice: true
      //})
   };
  }

  getFlights = ()=>{
    //fetch('/flights?${queryString}', { //akciove letenky na prvej strane
      fetch('/flights?from='+this.state.from+'&to='+this.state.from, {
         method: 'GET'
      })
      .then((response) => response.json())
      .then((responseJson) => { 
         this.setState({
            data: responseJson
         });
      })
      .then(
      () => {
        this.setState({ 
            getLoading: false
         });
        this.setState({ 
            getLoading: true
         });
  })
      .catch((error) => {
         console.error(error);
      });
  }

   componentDidMount(){
      this.getFlights();
   }

   renderSearchList = () => {
    return (this.state.data.map((flight) => {return (
        
        <Col xs="12" sm="12" md="12" lg="12" xl="12" key={flight._id} className='padding_row' >
        <Searchflight  
              id={flight.id}
              price={flight.price}
              from={flight.from}
              to={flight.to}
              airlines={flight.airlines}
              dateofdeparture={flight.dateofdeparture}
              timeofdeparture={flight.timeofdeparture}
              dateofarrival={flight.dateofarrival}
              timeofarrival={flight.timeofarrival} />
        </Col>
        
        )}));
  }

render() {
      return(
        <React.Fragment>
        <Container className="" fluid={true} >
        <Container>
        <Row>
        {this.renderSearchList()}
        </Row>
        </Container>
        </Container>
        </React.Fragment>
        );
   }

}
export default Searchlist;