import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import './App.js';
import MyNavbar from './navbar.js'
import NavbarSmall from './navbarsmall.js'
import Flightlist from './flightlist.js'
import Searchlist from './searchlist.js'
import Searchbar from './searchbar.js'
import { Row, Col, Jumbotron } from 'reactstrap';

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      showflightlist:true,
      from: '',
      to:''
   };
  }

  flightlistoff = (from, to) => {
    this.setState({
            from: from,
            to: to
         },()=> {
    this.setState({
            showflightlist:false
         });
  
  });
  }

render() {
      return (
        <React.Fragment>
        {this.state.showflightlist ? (
        <MyNavbar />
        ):(
        <NavbarSmall />
        )}
        <Searchbar flightlistoff={this.flightlistoff}/>
        <Row>
        {this.state.showflightlist ? (
        <Flightlist flightlistoff={this.flightlistoff} />
        ):(
        <Col  xs="6" sm="6" md="6" lg="6" xl="6">
        <Jumbotron>
        <Searchlist from={this.state.from} to={this.state.to} />
        </Jumbotron>
        </Col>
        )}
        </Row>
        </React.Fragment>
      )
 }  
}
export default App