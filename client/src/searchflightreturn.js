import React from 'react';
import './App.css';
import Searchflightdetail from './searchflightdetail.js'
import Searchflightdetailmiddle from './searchflightdetailmiddle.js'
import { Row, Col, Jumbotron } from 'reactstrap';

class Searchflight extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      price: this.props.price, //spocitane uz
      from: this.props.from,
      to: this.props.to,
      airlines: this.props.airlines,
      airlinesback: this.props.airlinesback,
      dateofdeparture: this.props.dateofdeparture,
      timeofdeparture: this.props.timeofdeparture,
      dateofarrival: this.props.dateofarival,
      timeofarrival: this.props.timeofarrival,
      dateofdepartureback: this.props.dateofdepartureback,
      timeofdepartureback: this.props.timeofdepartureback,
      dateofarrivalback: this.props.dateofarrivalback,
      timeofarrivalback: this.props.timeofarrivalback
   };
  }

  renderflight(){
    return (
        <React.Fragment><Jumbotron style={styles} >

        <Row>

        <Col xs="3" sm="3" md="3" lg="3" xl="3" className="my-auto">

        <div class="col text-center">
          <b>{this.state.price} €</b>
          </div>
        </Col>

        <Col xs="9" sm="9" md="9" lg="9" xl="9"  >
          <Searchflightdetail from={this.props.from}
                              to={this.props.to}
                              airlines={this.props.airlines}
                              dateofdeparture={this.props.dateofdeparture}
                              timeofdeparture={this.props.timeofdeparture}
                              dateofarrival={this.props.dateofarival}
                              timeofarrival={this.props.timeofarrival}

          <Searchflightdetailmiddle />

          <Searchflightdetail from={this.props.to}
                              to={this.props.from}
                              airlinesback={this.props.airlinesback}
                              dateofdepartureback={this.props.dateofdepartureback}
                              timeofdepartureback={this.props.timeofdepartureback}
                              dateofarrivalback={this.props.dateofarivalback}
                              timeofarrivalback={this.props.timeofarrivalback}
        </Col>
        </Row>
        </Jumbotron>
      </React.Fragment>
      )
  }

render() { 
          return (this.renderflight())

 }
}
export default Searchflight

var styles = {
  backgroundColor:'white'
};