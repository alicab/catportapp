import React from 'react';
import './App.css';
import { Row, Dropdown, DropdownToggle, DropdownMenu, DropdownItem, Button, Label, Input, FormGroup } from 'reactstrap';
import { GoPerson } from "react-icons/go";
import { GiBlackCat } from "react-icons/gi";

class Searchbardetail extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      persons: 1,
      cats: 1,
      class: 'Economy',
      way: 'Return',
      dropdownPersonOpen: false,
      dropdownCatOpen: false,
      dropdownWayOpen: false,
      dropdownClassOpen: false
    };
    this.togglePerson = this.togglePerson.bind(this);
    this.toggleCat = this.toggleCat.bind(this);
    this.toggleClass = this.toggleClass.bind(this);
    this.toggleWay = this.toggleWay.bind(this);
  }
  
  toggleCat() {
    this.setState({
      dropdownCatOpen: !this.state.dropdownCatOpen
    });
  }

  toggleClass() {
    this.setState({
      dropdownClassOpen: !this.state.dropdownClassOpen
    });
  }

  togglePerson() {
    this.setState({
      dropdownPersonOpen: !this.state.dropdownPersonOpen
    });
  }

  toggleWay() {
    this.setState({
      dropdownWayOpen: !this.state.dropdownWayOpen
    });
  }

  addCat = () => {
  	if(this.state.cats<=9){
  		this.setState({
      cats: this.state.cats+1
    });
  	}
  }

  addPerson = () => {
  	if(this.state.persons<=9){
  		this.setState({
      persons: this.state.persons+1
    });
  	}
  }

  subCat = () => {
  	if(this.state.cats>=2){
  		this.setState({
      cats: this.state.cats-1
    });
  	}
  }

  subPerson = () => {
  	if(this.state.persons>=2){
  		this.setState({
      persons: this.state.persons-1
    });
  	}
  }

changePersonCount = () => {
  	this.setState({
      dropdownPersonOpen: false
    });
  }

 changeCatCount = () => {
  	this.setState({
      dropdownCatOpen: false
    });
  } 

  changeClass = () => {
  	this.setState({
      dropdownClassOpen: false
    });
  } 

  changeFirstClass = () => {
  	this.setState({
      class: 'First Class'
    });
  } 

  changeBusinessClass = () => {
  	this.setState({
      class: 'Business Class'
    });
  } 

  changeWayOne = () => {
  	this.setState({
      way: 'One-way'
    });
  }

  changeWayReturn = () => {
  	this.setState({
      way: 'Return'
    });
  }

  changeWay = () => {
  	this.setState({
      dropdownWayOpen: false
    });
  }

  renderflight(){
    return (
        <React.Fragment>
        <Row>
        <Dropdown className="btn-outline-light" isOpen={this.state.dropdownPersonOpen} toggle={this.togglePerson.bind(this)}>
      <DropdownToggle caret>
        <GoPerson /> {this.state.persons}
      </DropdownToggle>
      <DropdownMenu>
        <DropdownItem header>Persons:
		<Button onClick={this.subPerson}>-</Button>
        {this.state.persons}
        <Button onClick={this.addPerson}>+</Button><br/>
        <Button onClick={this.changePersonCount}>Done</Button>
      </DropdownItem>
      </DropdownMenu>
    </Dropdown>

       <Dropdown className="btn-outline-light" style={styles} isOpen={this.state.dropdownCatOpen} toggle={this.toggleCat.bind(this)}>
      <DropdownToggle caret>
        <GiBlackCat /> {this.state.cats}
      </DropdownToggle>
      <DropdownMenu>
        <DropdownItem header>Cats:
        <Button onClick={this.subCat}>-</Button>
        {this.state.cats}
        <Button onClick={this.addCat}>+</Button><br/>
        <Button onClick={this.changeCatCount}>Done</Button>
        </DropdownItem>
      </DropdownMenu>
    </Dropdown>

    <Dropdown className="btn-outline-light" style={styles} isOpen={this.state.dropdownClassOpen} toggle={this.toggleClass.bind(this)}>
      <DropdownToggle caret>
        {this.state.class}
      </DropdownToggle>
      <DropdownMenu>
        <DropdownItem header>
        <FormGroup>
        <Label for="exampleSelect">Class:</Label>
        <Input type="select" name="select" id="select">
          <option>Economy</option>
          <option>Premium Economy</option>
          <option onClick={this.changeBusinessClass}>Business</option>
          <option onClick={this.changeFirstClass} >First Class</option>
          <option>Mixed</option>
        </Input>
      </FormGroup>
        <Button onClick={this.changeClass}>Done</Button>
        </DropdownItem>
      </DropdownMenu>
    </Dropdown>

    <Dropdown className="btn-outline-light" style={styles} isOpen={this.state.dropdownWayOpen} toggle={this.toggleWay.bind(this)}>
      <DropdownToggle caret>
        {this.state.way}
      </DropdownToggle>
      <DropdownMenu>
        <DropdownItem header>
        <FormGroup>
        <Input type="select" name="select" id="select">
          <option onClick={this.changeWayOne}>One-way</option>
          <option onClick={this.changeWayReturn} >Return</option>
        </Input>
      </FormGroup>
        <Button onClick={this.changeWay}>Done</Button>
        </DropdownItem>
      </DropdownMenu>
    </Dropdown>
        </Row>
      </React.Fragment>
      )
  }

render() { 
          return (this.renderflight())

 }
}
export default Searchbardetail

var styles = {
  backgroundColor:'white',
  color:'black'
};
