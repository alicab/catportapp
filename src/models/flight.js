let mongoose = require('mongoose')

let flightSchema = new mongoose.Schema({
  from: String,
  to: String,
  price: String,
  reducedprice: Boolean,
  airlines: String,
  imgname: String,
  dateofdeparture: String,
  timeofdeparture: String,
  dateofarrival: String,
  timeofarrival: String, 
  numberofseats: Number, //cislo delitelne 6
  purchaser: [{
  	firstname: String,
  	lastname: String,
  	dateofbirth: String,
  	seat: Number,
  	cat: [{
  		name: String
  	}],
  	passenger: [{
  		firstname: String,
  		lastname: String,
  		dateofbirth: String,
  		seat: Number
  	}]
  }]

})

module.exports = mongoose.model('Flight', flightSchema)